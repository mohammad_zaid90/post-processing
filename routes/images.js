'use strict';

const imageClass = require('../controllers/images');

exports.imageRoutes = () => {

    const imagesCtrl = new imageClass();

    app.get('/images/all', (req, res, next) => imagesCtrl.getAll(req, res, next));
}
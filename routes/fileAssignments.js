'use strict';

const fileAssignmentsClass = require('../controllers/fileAssignments');

exports.fileAssignmentsRoutes = () => {

    const fileAssignmentsClassCtrl = new fileAssignmentsClass();

    app.post('/fileAssignments/start', (req, res, next) => fileAssignmentsClassCtrl.start(req, res, next));
}
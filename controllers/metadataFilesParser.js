'use strict';

const downloaderClass = require('./downloader');

class metadataFilesParser {
    constructor() {
        this.downloaderCtrl = new downloaderClass();
    }

    /**
     * Read and parse meta files
     * 1. Download/get metadata files from S3
     * 2. Parse downloaded files
     * @param {Array} metadataFilesBucketAndKeys 
     */
    readMetadataFile(metadataFilesBucketAndKeys) {
        const self = this;
        if (!metadataFilesBucketAndKeys) return Promise.reject('Invalid bucket and keys!');

        let imagesMetadata, urlsPairs;

        // Assuming to handle 2 metadata files
        const downloadPromises = metadataFilesBucketAndKeys.map(file => {
            return self.downloaderCtrl.downloadFile(file.bucket, file.key)
                .then(data => {

                    if (file.key.includes('meta')) {
                        imagesMetadata = JSON.parse(data.Body.toString('utf-8'));
                    } else {
                        urlsPairs = JSON.parse(data.Body.toString('utf-8'));
                    }
                })
        });

        return Promise.all(downloadPromises)
            .then(() => {
                return { imagesMetadata, urlsPairs };
            });
    }

}

module.exports = metadataFilesParser;
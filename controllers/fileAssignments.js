'use strict';

const config = require('config'),
    _ = require('lodash'),
    responseErrors = require('http-errors'),
    crc = require('crc');

const imageCollection = require('../schemas/image'),
    metadataFileCollection = require('../schemas/metadataFile');

const downloaderClass = require('./downloader'),
    metadataFilesParser = require('../controllers/metadataFilesParser');

const utilities = require('../lib/utilities');

const fileStatuses = {
    1: "good",
    2: "bad",
    3: "inprocess"
}

class fileAssignment {
    constructor() {
        this.downloaderCtrl = new downloaderClass();
        this.metadataFilesParserCtrl = new metadataFilesParser();
        this.imagesS3Keys = [];
    }

    // Start the post-processing logic by performing the file assignments for each image file
    async start(req, res, next) {

        res.send('Starting post processing..');

        const self = this;

        try {
            const metadataFilesBucketAndKeys = await self.getUploadedImagesMetadataFilesKeys();

            // Metadata files as json objects
            const { imagesMetadata, urlsPairs } = await self.metadataFilesParserCtrl.readMetadataFile(metadataFilesBucketAndKeys);

            const imagesMetadataByFileName = _.keyBy(imagesMetadata, 'fileName');

            const recievedImagesBucketsAndKeys = await self.getUploadedImagesS3Keys();

            // Prepare file assinment promises
            const calculationPromises = recievedImagesBucketsAndKeys.map(image => {
                let status;

                return self.downloaderCtrl.downloadFile(image.bucket, image.key)
                    .then(recievedImage => {

                        const imageMetadata = imagesMetadataByFileName[image.recievedFileName];

                        // Start perform file assignments
                        status = self.calculateFileCRC(recievedImage, imageMetadata);
                        status = self.calculateFileSumOfBytes(recievedImage, imageMetadata);
                        status = self.calculateFileSize(recievedImage, imageMetadata);
                        return self.requestDataServer();
                    })
                    .then(() => {
                        return self.requestUrls(urlsPairs);
                    })
                    .then(status => {
                        // Upadate the last status result
                        return imageCollection.updateOne({ key: image.key }, { status });
                    })
                    .then(res => {
                        logger.info(`[FileAssignments] Image updated ${JSON.stringify(res)}`);
                    })
                    .catch(err => {
                        logger.error(`[FileAssignments] Failed update image status to ${status}. ${err}`);
                    });
            });

            return Promise.all(calculationPromises)
                .then(() => {
                    logger.info('Post processing done!');
                });
        } catch (err) {
            return next(new responseErrors.BadRequest(JSON.stringify(err)));
        }
    }

    async getUploadedImagesS3Keys() {
        return await imageCollection.find({ status: 'inProcess' }, { bucket: 1, key: 1, recievedFileName: 1 }).sort({ updatedAt: -1 }).limit(5);
    }

    async getUploadedImagesMetadataFilesKeys() {
        return await metadataFileCollection.find({}, { bucket: 1, key: 1 }).sort({ updatedAt: -1 }).limit(2);
    }

    calculateFileCRC(recievedImage, imageMetadata) {
        const recievedImageCrc = crc.crc32(recievedImage.Body).toString(16);
        return (recievedImageCrc === imageMetadata.fileCRC) ? fileStatuses['1'] : fileStatuses['2'];
    }

    calculateFileSumOfBytes(recievedImage, imageMetadata) {
        return (recievedImage.ContentLength === imageMetadata.fileSumOfBytes) ? fileStatuses['1'] : fileStatuses['2'];
    }

    calculateFileSize(recievedImage, imageMetadata) {
        return (recievedImage.ContentLength === imageMetadata.fileSumOfBytes) ? fileStatuses['1'] : fileStatuses['2'];
    }

    async requestDataServer() {
        const dataServerConfig = Object.assign({
            relPath: 'data',
            method: 'GET'
        }, config.restParams.dataServer);

        const response = await utilities.sendRequest(dataServerConfig);

        return (response.statusCode === 200) ? fileStatuses['1'] : fileStatuses['2'];
    }

    requestUrls(urlsPairs) {
        const sendRequests = Object.keys(urlsPairs).map(url => {
            // TO-DO validate the url

            return utilities.sendRequest({
                uri: url,
                method: 'POST',
                body: urlsPairs[url] // Payload
            })
        });

        return Promise.all(sendRequests)
            .then(() => {
                return fileStatuses['1'];
            })
            .catch(err => {
                logger.error(`[FileAssignments] Failed to send all the urls. ${err}`);
                return fileStatuses['2']
            });
    }
}

module.exports = fileAssignment;
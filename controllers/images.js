'use strict';

const imageCollection = require('../schemas/image');

class imagesClass {

    constructor() { }

    // Get all images were stored to DB
    async getAll(req, res, next) {
        const dbImages = await imageCollection.find({}, { _id: 0, recievedFileName: 1, size: 1, status: 1 });
        res.send(dbImages);
    }
}

module.exports = imagesClass;
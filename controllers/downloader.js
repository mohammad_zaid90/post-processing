'use strict';

const awsHandlerClass = require('../lib/awsHandlerClass');

class downloader {
    constructor() {
        this.awsHandler = new awsHandlerClass();
        this.S3 = this.awsHandler.createAwsService('S3', 'eu-central-1');
    }

    async downloadFile(bucket, key) {
        const params = {
            Bucket: bucket,
            Key: key
        };

        const data = await this.S3.getObject(params).promise();

        return data;
    }
}

module.exports = downloader;
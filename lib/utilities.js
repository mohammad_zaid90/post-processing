'use strict';

const request = require('request'),
    util = require('util');

const requestPromise = util.promisify(request);

// Perform a generic request
function sendRequest(requestData) {
    requestData.headers = requestData.headers || {};
    requestData.timeout = requestData.timeout || 10000; // MS

    requestData.uri += requestData.relPath;

    return requestPromise(requestData);
}

module.exports = {
    sendRequest
};